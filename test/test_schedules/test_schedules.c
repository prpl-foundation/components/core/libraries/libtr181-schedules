/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <unistd.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>

#include "../common/common_functions.h"
#include "test_schedules.h"

#include "schedule.h"
#include "common.h"
#include "schedule_priv.h"
#include "schedule_helpers.h"
#include "schedule_object.h"

#define STR_EMPTY(_x) ((_x) == NULL || (_x)[0] == '\0')

static void set_status(amxd_object_t* schedule_object, const char* new_status) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, schedule_object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_set_value(cstring_t, &trans, "Status", new_status);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), amxd_status_ok);
    amxd_trans_clean(&trans);
}

static char* create_object(const char* alias, bool enable, const char* day, const char* start_time, uint32_t duration, bool inverse) {
    amxd_object_t* root_obj = amxd_dm_findf(amxut_bus_dm(), "Schedules");
    amxd_object_t* object = NULL;
    amxd_trans_t trans;
    char* path = NULL;

    amxd_trans_init(&trans);
    assert_false(STR_EMPTY(day));
    assert_false(STR_EMPTY(alias));

    amxd_trans_set_attr(&trans, amxd_tattr_change_priv, true);
    amxd_trans_select_pathf(&trans, "Schedules.Schedule");
    amxd_trans_add_inst(&trans, 0, alias);
    amxd_trans_set_value(bool, &trans, "Enable", enable);
    amxd_trans_set_value(uint32_t, &trans, "Duration", duration);
    amxd_trans_set_value(cstring_t, &trans, "Alias", alias);
    amxd_trans_set_value(cstring_t, &trans, "StartTime", start_time);
    amxd_trans_set_value(cstring_t, &trans, "Day", day);
    amxd_trans_set_value(bool, &trans, "InverseMode", inverse);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);

    object = amxd_object_findf(root_obj, "Schedule.[Alias == \"%s\"].", alias);
    assert_non_null(object);

    path = amxd_object_get_path(object, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    amxd_trans_clean(&trans);
    return path;
}

static void status_update(schedule_status_t new_status, void* userdata) {
    function_called();
    check_expected(new_status);
    check_expected(userdata);
}

static int test_setup(void** state, bool load_odl) {
    amxut_bus_setup(state);
    if(load_odl) {
        test_setup_parse_odl(DUMMY_TEST_ODL);
    }
    schedule_initialize();

    return 0;
}

int test_setup_regular(void** state) {
    return test_setup(state, true);
}

int test_setup_no_dm(void** state) {
    return test_setup(state, false);
}

int test_teardown(void** state) {
    schedule_cleanup();
    amxut_bus_teardown(state);
    return 0;
}

void test_existing_object(UNUSED void** state) {
    schedule_t* scheduler = NULL;
    void* userdata = (void*) "TEST";
    char* path = NULL;
    amxd_object_t* schedule_object = amxd_dm_findf(amxut_bus_dm(), "Schedules.Schedule.1.");
    assert_non_null(schedule_object);

    path = amxd_object_get_path(schedule_object, AMXD_OBJECT_INDEXED);

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_DISABLED);
    expect_value(status_update, userdata, userdata);
    scheduler = schedule_open(path, status_update, userdata);
    assert_non_null(scheduler);
    assert_int_equal(amxc_htable_size(&(scheduler->schedule_objects)), 1);

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_INIT);
    expect_value(status_update, userdata, userdata);
    set_status(schedule_object, "Init");
    amxut_bus_handle_events();

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_ACTIVE);
    expect_value(status_update, userdata, userdata);
    set_status(schedule_object, "Active");
    amxut_bus_handle_events();

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_DISABLED);
    expect_value(status_update, userdata, userdata);
    set_status(schedule_object, "Disabled");
    amxut_bus_handle_events();

    schedule_close(&scheduler);
    free(path);
}

void test_new_object(UNUSED void** state) {
    schedule_t* scheduler = NULL;
    void* userdata = (void*) "TEST2";
    amxd_object_t* schedule_object = NULL;
    amxd_trans_t trans;
    const char* alias = "object2";
    bool enable = false;
    const char* day = "Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday";
    const char* start_time = "";
    char* object_alias = NULL;
    uint32_t duration = 0;
    bool inverse = false;
    char* path = NULL;

    path = create_object(alias, enable, day, start_time, duration, inverse);

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_DISABLED);
    expect_value(status_update, userdata, userdata);
    scheduler = schedule_open(path, status_update, userdata);
    assert_non_null(scheduler);
    assert_int_equal(amxc_htable_size(&scheduler->schedule_objects), 1);

    schedule_object = amxd_dm_findf(amxut_bus_dm(), "Schedules.Schedule.2.");
    assert_non_null(schedule_object);
    object_alias = amxd_object_get_value(cstring_t, schedule_object, "Alias", NULL);
    assert_string_equal(object_alias, alias);
    free(object_alias);

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_ERROR);
    expect_value(status_update, userdata, userdata);
    set_status(schedule_object, "Error");
    amxut_bus_handle_events();

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_INACTIVE);
    expect_value(status_update, userdata, userdata);
    set_status(schedule_object, "Inactive");
    amxut_bus_handle_events();

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_STACK_DISABLED);
    expect_value(status_update, userdata, userdata);
    set_status(schedule_object, "StackDisabled");
    amxut_bus_handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Schedules.Schedule");
    amxd_trans_del_inst(&trans, 0, alias);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();

    amxd_trans_clean(&trans);

    schedule_close(&scheduler);
    free(path);
}

void test_overlap(UNUSED void** state) {
    schedule_t* scheduler = NULL;
    void* userdata = (void*) "TEST2";
    const char* day = "Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday";
    amxd_object_t* item1 = NULL;
    amxd_object_t* item2 = NULL;
    amxc_string_t path;
    char* path_item1 = NULL;
    char* path_item2 = NULL;

    amxc_string_init(&path, 0);
    path_item1 = create_object("item1", true, day, "15:00", 6600 /* 16:50 */, false);
    path_item2 = create_object("item2", true, day, "16:30", 5400 /* 18:00 */, false);
    amxc_string_setf(&path, "%s,%s", path_item1, path_item2);

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_DISABLED);
    expect_value(status_update, userdata, userdata);
    scheduler = schedule_open(amxc_string_get(&path, 0), status_update, userdata);
    assert_non_null(scheduler);
    assert_int_equal(amxc_htable_size(&scheduler->schedule_objects), 2);

    item1 = amxd_dm_findf(amxut_bus_dm(), "Schedules.Schedule.3.");
    item2 = amxd_dm_findf(amxut_bus_dm(), "Schedules.Schedule.4.");
    assert_non_null(item1);
    assert_non_null(item2);

    //     15:00              16:50         18:00
    //       |----------|----------|----------|
    // item1 |**********|********--|----------|
    // item2 |----------|----******|**********|
    //                     16:30

    // Set time to 14:59 (item1: Inactive ; item2: Inactive)
    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_INACTIVE);
    expect_value(status_update, userdata, userdata);
    set_status(item1, "Inactive");
    set_status(item2, "Inactive");
    amxut_bus_handle_events();

    // Set time to 15:00 (item1: Active ; item2: Inactive)
    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_ACTIVE);
    expect_value(status_update, userdata, userdata);
    set_status(item1, "Active");
    amxut_bus_handle_events();

    // Set time to 16:30 (item1: Active ; item2: Active)
    set_status(item2, "Active");
    amxut_bus_handle_events();

    // Set time to 16:50 (item1: Inactive ; item2: Active)
    set_status(item1, "Inactive");
    amxut_bus_handle_events();

    // Set time to 18:00 (item1: Inactive ; item2: Inactive)
    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_INACTIVE);
    expect_value(status_update, userdata, userdata);
    set_status(item2, "Inactive");
    amxut_bus_handle_events();

    schedule_close(&scheduler);
    amxc_string_clean(&path);
    free(path_item1);
    free(path_item2);
}

void test_adjacent(UNUSED void** state) {
    schedule_t* scheduler = NULL;
    void* userdata = NULL;
    const char* day = "Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday";
    amxd_object_t* item3 = NULL;
    amxd_object_t* item4 = NULL;
    amxc_string_t path;
    char* path_item3 = NULL;
    char* path_item4 = NULL;

    amxc_string_init(&path, 0);
    path_item3 = create_object("item3", true, day, "10:00", 3600 /* 11:00 */, false);
    path_item4 = create_object("item4", true, day, "11:00", 1800 /* 11:30 */, false);
    amxc_string_setf(&path, "%s,%s", path_item3, path_item4);

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_DISABLED);
    expect_value(status_update, userdata, userdata);
    scheduler = schedule_open(amxc_string_get(&path, 0), status_update, userdata);
    assert_non_null(scheduler);
    assert_int_equal(amxc_htable_size(&scheduler->schedule_objects), 2);

    item3 = amxd_dm_findf(amxut_bus_dm(), "Schedules.Schedule.5.");
    item4 = amxd_dm_findf(amxut_bus_dm(), "Schedules.Schedule.6.");
    assert_non_null(item3);
    assert_non_null(item4);

    //     10:00                11:00                12:00
    //       |--------------------|--------------------|
    // item3 |********************|--------------------|
    // item4 |--------------------|**********----------|
    //                                    11:30

    // Set time to 14:59 (item3: Inactive ; item4: Inactive)
    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_INACTIVE);
    expect_value(status_update, userdata, userdata);
    set_status(item3, "Inactive");
    set_status(item4, "Inactive");
    amxut_bus_handle_events();

    // Set time to 15:00 (item3: Active ; item4: Inactive)
    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_ACTIVE);
    expect_value(status_update, userdata, userdata);
    set_status(item3, "Active");
    amxut_bus_handle_events();

    // Set time to 16:00 (item3: Inactive ; item4: Active)
    set_status(item4, "Active");
    set_status(item3, "Inactive");
    amxut_bus_handle_events();

    // Set time to 16:30 (item3: Inactive ; item4: Inactive)
    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_INACTIVE);
    expect_value(status_update, userdata, userdata);
    set_status(item4, "Inactive");
    amxut_bus_handle_events();

    schedule_close(&scheduler);
    amxc_string_clean(&path);
    free(path_item3);
    free(path_item4);
}

void test_csv_paths(UNUSED void** state) {
    schedule_t* scheduler = NULL;
    amxd_object_t* item = amxd_dm_findf(amxut_bus_dm(), "Schedules.Schedule.6.");
    amxd_trans_t trans;

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_INACTIVE);
    expect_value(status_update, userdata, NULL);
    scheduler = schedule_open("Schedules.Schedule.3,Schedules.Schedule.4,Schedules.Schedule.5,Schedules.Schedule.6", status_update, NULL);
    assert_non_null(scheduler);
    assert_int_equal(amxc_htable_size(&scheduler->schedule_objects), 4);

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_STACK_DISABLED);
    expect_value(status_update, userdata, NULL);
    set_status(item, "StackDisabled");
    amxut_bus_handle_events();

    schedule_close(&scheduler);
    amxut_bus_handle_events();

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_INACTIVE);
    expect_value(status_update, userdata, NULL);
    scheduler = schedule_open("Schedules.Schedule.3,Schedules.Schedule.4,Schedules.Schedule.5", status_update, NULL);
    amxut_bus_handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Schedules.Schedule");
    amxd_trans_del_inst(&trans, 0, "item1");
    amxd_trans_del_inst(&trans, 0, "item3");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    schedule_close(&scheduler);
}

void test_disable_schedules(UNUSED void** state) {
    schedule_t* scheduler = NULL;
    amxd_object_t* item1 = amxd_dm_findf(amxut_bus_dm(), "Schedules.Schedule.1.");
    amxd_object_t* item4 = amxd_dm_findf(amxut_bus_dm(), "Schedules.Schedule.4.");
    amxd_object_t* item6 = amxd_dm_findf(amxut_bus_dm(), "Schedules.Schedule.6.");
    assert_non_null(item1);
    assert_non_null(item4);
    assert_non_null(item6);

    set_status(item6, "Inactive");
    amxut_bus_handle_events();

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_INACTIVE);
    expect_value(status_update, userdata, NULL);
    scheduler = schedule_open("Schedules.Schedule.1,Schedules.Schedule.4,Schedules.Schedule.6", status_update, NULL);
    assert_non_null(scheduler);
    assert_int_equal(amxc_htable_size(&scheduler->schedule_objects), 3);

    expect_function_call(status_update);
    expect_value(status_update, new_status, SCHEDULE_STATUS_STACK_DISABLED);
    expect_value(status_update, userdata, NULL);
    set_status(item1, "StackDisabled");
    set_status(item4, "StackDisabled");
    set_status(item6, "StackDisabled");
    amxut_bus_handle_events();

    schedule_close(&scheduler);
    amxut_bus_handle_events();
}

void test_invalid_schedules(UNUSED void** state) {
    schedule_t* scheduler = NULL;
    scheduler = schedule_open("Schedules.Schedule.100", status_update, NULL);
    assert_null(scheduler);

    schedule_close(&scheduler);
    amxut_bus_handle_events();
}

void test_no_datamodel(UNUSED void** state) {
    schedule_t* scheduler = NULL;
    scheduler = schedule_open("Schedules.Schedule.1", status_update, NULL);
    assert_null(scheduler);

    schedule_close(&scheduler);
    amxut_bus_handle_events();
}

void test_get_terminated_path(UNUSED void** state) {
    char* result = NULL;

    assert_null(get_terminated_path(NULL, false));
    assert_null(get_terminated_path(NULL, true));
    assert_null(get_terminated_path("", false));
    assert_null(get_terminated_path("", true));

    result = get_terminated_path("dsfljkladsjfl", false);
    assert_string_equal(result, "dsfljkladsjfl");
    free(result);

    result = get_terminated_path("Device.IP.Interface.2", false);
    assert_string_equal(result, "Device.IP.Interface.2");
    free(result);

    result = get_terminated_path("Device.IP.Interface.2", true);
    assert_string_equal(result, "Device.IP.Interface.2.");
    free(result);

    result = get_terminated_path("Device.IP.Interface.2.", false);
    assert_string_equal(result, "Device.IP.Interface.2");
    free(result);

    result = get_terminated_path("Device.IP.Interface.2.", true);
    assert_string_equal(result, "Device.IP.Interface.2.");
    free(result);
}
