/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__SCHEDULER_H__)
#define __SCHEDULER_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
   @defgroup schedule schedule.h - Schedule API
   @{

   @brief
   The Schedule API consists of a small set of functions to easily manage one or more Schedule objects.

   @warning
   It is up to the user of this library to make sure that the Schedules datamodel is available.
   When using this library without the DM, each @ref schedule_open call will return NULL.

   @details
   This library can be used to track and get updates on Schedule status updates.
   Instead of having to get the initial status and/or subscribe on a Schedules.Schedule object this library makes this task a lot easier.

   If you already used libnetmodel, the API will feel very intuitive
   since this library is using the same principles to initialize and use with the opening and closing functions.

 */

typedef enum _schedule_status {
    SCHEDULE_STATUS_EMPTY,
    SCHEDULE_STATUS_INIT,
    SCHEDULE_STATUS_ACTIVE,
    SCHEDULE_STATUS_INACTIVE,
    SCHEDULE_STATUS_DISABLED,
    SCHEDULE_STATUS_STACK_DISABLED,
    SCHEDULE_STATUS_ERROR,
} schedule_status_t;

/**
   @brief
   Callback function that will be called when a schedule object has a changed state.

   @param status: Enum representing the new state
                  Can be one of the following:
                  - SCHEDULE_STATUS_EMPTY:          No schedule instances configured
                  - SCHEDULE_STATUS_INIT:           Time is not synced properly
                  - SCHEDULE_STATUS_ACTIVE:         One or more schedule instances are active
                  - SCHEDULE_STATUS_INACTIVE:       One or more schedule instances are inactive,
                                                    without there being one schedule instance that is active
                  - SCHEDULE_STATUS_DISABLED:       All schedule instances are disabled
                  - SCHEDULE_STATUS_STACK_DISABLED: The schedules stack is disabled
                  - SCHEDULE_STATUS_ERROR:          Something went wrong
   @param priv:   Private data, passed to the callback function
 */
typedef void (* schedule_callback_t) (schedule_status_t status, void* priv);

/**
   @brief
   Opaque handle for an opened schedule.

   @details
   This type of object is returned by schedule_open() and needs to be passed to schedule_close() exactly once.
   @see schedule_open()
   @see schedule_close()
 */
typedef struct schedule schedule_t;

/**
   @brief
   Initialize the schedule library.
   Must be called before running any other function from this library.

   @return true if succesfull, false otherwise.
 */
bool schedule_initialize(void);

/**
   @brief
   Cleanup the schedule library.

   @details
   Releases internal structures.
 */
void schedule_cleanup(void);

/**
   @brief
   Open a schedule to get the current status of one or more Schedule objects and get notified whenever that status changes.
   @warning When the Schedules datamodel is not present all schedule_open calls will return NULL.

   @param path A comma-separated list of strings. Each list item MUST be the Path Name of a row in the Schedules.Schedule. datamodel.
               Cannot be empty.
   @param cb The callback function to be called with the initial status and later on whenever the status changes.
             This function will be called for the first time before the call to schedule_open() finishes.
             If the status changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the cb ( @ref schedule_callback_t )
   @return A pointer to an opaque schedule object if the open() call succeeded, or NULL when the open() call fails.
 */
schedule_t* schedule_open(const char* path, schedule_callback_t cb, void* userdata);

/**
   @brief
   Close an opened schedule

   @param schedule  A NULL-pointer (NULL-pointers are ignored) or a pointer to a pointer to an opaque schedule object returned by a successful call
             to schedule_open(). When this function returns, this pointer is no longer valid.
 */
void schedule_close(schedule_t** schedule);

/**
   @}
 */

#ifdef __cplusplus
}
#endif

#endif
