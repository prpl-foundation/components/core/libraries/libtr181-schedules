/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "schedule_priv.h"

#include <string.h>
#include <stdlib.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxd/amxd_common.h>

#include "common.h"
#include "global.h"
#include "schedule_helpers.h"
#include "schedule_object.h"

static amxc_llist_t schedule_objects;

static void subscription_changed_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* status = NULL;
    schedule_object_t* obj = NULL;
    const char* new_status = NULL;
    char* unterminated_path = NULL;

    when_null(data, exit);
    unterminated_path = get_terminated_path(GET_CHAR(data, "path"), false);
    status = GETP_ARG(data, "parameters." STATUS);
    when_null(status, exit);
    when_str_empty(unterminated_path, exit);

    obj = schedule_object_find(unterminated_path);
    when_null_trace(obj, exit, ERROR, "Could not find internal object for %s", unterminated_path);

    new_status = GET_CHAR(status, "to");
    when_str_empty(new_status, exit);
    obj->status = schedule_status_from_string(new_status);
    SAH_TRACEZ_INFO(ME, "%s Changed to %s", unterminated_path, new_status);

    // We have to recalculate the status for all schedule_t* objects that use this Schedule path
    update_schedules(unterminated_path);

exit:
    free(unterminated_path);
    SAH_TRACEZ_OUT(ME);
}

schedule_object_t* schedule_object_find(const char* unterminated_path) {
    SAH_TRACEZ_IN(ME);
    schedule_object_t* obj = NULL;

    when_str_empty_trace(unterminated_path, exit, ERROR, "Given path is empty");

    amxc_llist_iterate(it, &schedule_objects) {
        obj = amxc_llist_it_get_data(it, schedule_object_t, it);
        when_true(strcmp(unterminated_path, obj->unterminated_path) == 0, exit);
    }
    obj = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return obj;
}

int schedule_object_new(schedule_object_t** obj, const char* unterminated_path) {
    SAH_TRACEZ_IN(ME);
    int res = -1;
    amxb_bus_ctx_t* ctx = amxb_be_who_has(SCHEDULES);
    const char* status = NULL;
    amxc_string_t changed_expression_str;
    const char* alias = NULL;
    char* terminated_path = NULL;
    amxc_var_t ret;

    amxc_string_init(&changed_expression_str, 0);

    amxc_var_init(&ret);

    when_null_trace(ctx, exit, ERROR, "Could not find " SCHEDULES " context");
    when_str_empty_trace(unterminated_path, exit, ERROR, "Empty path is given");

    terminated_path = get_terminated_path(unterminated_path, true);
    res = amxb_get(ctx, terminated_path, 0, &ret, 5);
    when_failed_trace(res, exit, ERROR, "Could not get object %s (%s)", unterminated_path, amxd_status_string(res));

    *obj = (schedule_object_t*) calloc(1, sizeof(schedule_object_t));
    when_null_trace(*obj, exit, ERROR, "Failed to allocate schedule object");

    status = GETP_CHAR(&ret, "0.0.Status");
    alias = GETP_CHAR(&ret, "0.0.Alias");
    amxc_string_setf(&changed_expression_str, "notification in ['dm:object-changed'] and \
                                       (object matches '" SCHEDULES "\\." SCHEDULE "\\.%s\\.$') and \
                                       contains('parameters." STATUS "')", alias);

    res = amxb_subscription_new(&(*obj)->watcher, ctx, SCHEDULES, amxc_string_get(&changed_expression_str, 0), subscription_changed_cb, NULL);
    if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create " SCHEDULE " status change subscription");
        schedule_object_delete(*obj);
        *obj = NULL;
        goto exit;
    }

    (*obj)->unterminated_path = strdup(unterminated_path);
    (*obj)->status = schedule_status_from_string(status);
    amxc_llist_it_init(&(*obj)->it);
    amxc_llist_append(&schedule_objects, &(*obj)->it);

exit:
    free(terminated_path);
    amxc_var_clean(&ret);
    amxc_string_clean(&changed_expression_str);
    SAH_TRACEZ_OUT(ME);
    return res;
}

void schedule_object_delete(schedule_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    when_null(obj, exit);
    amxc_llist_it_take(&obj->it);
    amxb_subscription_delete(&(obj->watcher));
    free(obj->unterminated_path);
    free(obj);
    obj = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void schedule_object_it_delete(amxc_llist_it_t* it) {
    SAH_TRACEZ_IN(ME);
    schedule_object_t* obj = amxc_llist_it_get_data(it, schedule_object_t, it);
    schedule_object_delete(obj);
    SAH_TRACEZ_OUT(ME);
}

void init_schedule_objects(void) {
    SAH_TRACEZ_IN(ME);
    amxc_llist_init(&schedule_objects);
    SAH_TRACEZ_OUT(ME);
}

void cleanup_schedule_objects(void) {
    SAH_TRACEZ_IN(ME);
    amxc_llist_clean(&schedule_objects, schedule_object_it_delete);
    SAH_TRACEZ_OUT(ME);
    return;
}