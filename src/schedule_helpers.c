/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "common.h"
#include "schedule_helpers.h"

#define SCHEDULE_STATUS_STR_EMPTY          "Empty"
#define SCHEDULE_STATUS_STR_INIT           "Init"
#define SCHEDULE_STATUS_STR_ACTIVE         "Active"
#define SCHEDULE_STATUS_STR_INACTIVE       "Inactive"
#define SCHEDULE_STATUS_STR_DISABLED       "Disabled"
#define SCHEDULE_STATUS_STR_STACK_DISABLED "StackDisabled"
#define SCHEDULE_STATUS_STR_ERROR          "Error"

const char* schedule_status_to_string(schedule_status_t status) {
    SAH_TRACEZ_IN(ME);
    static const char* str[] = {SCHEDULE_STATUS_STR_EMPTY, SCHEDULE_STATUS_STR_INIT, SCHEDULE_STATUS_STR_ACTIVE, SCHEDULE_STATUS_STR_INACTIVE,
        SCHEDULE_STATUS_STR_DISABLED, SCHEDULE_STATUS_STR_STACK_DISABLED, SCHEDULE_STATUS_STR_ERROR};

    SAH_TRACEZ_OUT(ME);
    return (status >= 0 && status < SCHEDULE_STATUS_ERROR) ? str[status] : NULL;
}

schedule_status_t schedule_status_from_string(const char* status) {
    SAH_TRACEZ_IN(ME);
    schedule_status_t c = SCHEDULE_STATUS_EMPTY;

    when_str_empty(status, exit);

    for(; c < SCHEDULE_STATUS_ERROR; c++) {
        const char* result = schedule_status_to_string(c);
        if(strcmp(status, result) == 0) {
            break;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return c;
}

char* get_terminated_path(const char* path, bool terminate) {
    char* altered_path = NULL;
    bool path_is_terminated = false;

    when_str_empty(path, exit);

    path_is_terminated = strncmp(path + strlen(path) - 1, ".", 1) == 0;

    if(terminate && !path_is_terminated) {
        amxc_string_t path_dot_ended;
        amxc_string_init(&path_dot_ended, 0);
        amxc_string_setf(&path_dot_ended, "%s.", path);
        altered_path = amxc_string_take_buffer(&path_dot_ended);
        amxc_string_clean(&path_dot_ended);
    } else if(!terminate && path_is_terminated) {
        altered_path = strdup(path);
        when_str_empty(altered_path, exit);
        altered_path[strlen(path) - 1] = '\0';
    } else {
        altered_path = strdup(path);
    }

exit:
    return altered_path;
}