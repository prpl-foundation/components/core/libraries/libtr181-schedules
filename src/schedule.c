/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include "schedule.h"
#include "common.h"
#include "global.h"
#include "schedule_priv.h"
#include "schedule_helpers.h"
#include "schedule_object.h"

void calculate_status(schedule_t* schedule) {
    SAH_TRACEZ_IN(ME);
    uint32_t nr_active = 0;
    uint32_t nr_inactive = 0;
    uint32_t nr_disabled = 0;
    uint32_t nr_error = 0;
    uint32_t nr_init = 0;
    bool stack_disabled = false;
    uint32_t total_nr_entries = amxc_htable_size(&schedule->schedule_objects);
    schedule_status_t global_status = SCHEDULE_STATUS_EMPTY;

    when_null(schedule, exit);
    when_true(total_nr_entries == 0, skip_calculations);

    amxc_htable_iterate(it, &schedule->schedule_objects) {
        schedule_object_t* obj = NULL;
        const char* unterminated_path = amxc_htable_it_get_key(it);
        when_str_empty_trace(unterminated_path, exit, ERROR, "Path not found");

        obj = schedule_object_find(unterminated_path);
        when_null_trace(obj, exit, ERROR, "Schedule Object not found for path %s", unterminated_path);
        if(obj->status == SCHEDULE_STATUS_ACTIVE) {
            nr_active++;
        } else if(obj->status == SCHEDULE_STATUS_INACTIVE) {
            nr_inactive++;
        } else if(obj->status == SCHEDULE_STATUS_DISABLED) {
            nr_disabled++;
        } else if(obj->status == SCHEDULE_STATUS_ERROR) {
            nr_error++;
        } else if(obj->status == SCHEDULE_STATUS_INIT) {
            nr_init++;
        } else if(obj->status == SCHEDULE_STATUS_STACK_DISABLED) {
            stack_disabled = true;
            break;
        }
    }

    if(stack_disabled) {
        global_status = SCHEDULE_STATUS_STACK_DISABLED;
    } else if(nr_init == total_nr_entries) {
        global_status = SCHEDULE_STATUS_INIT;
    } else if(nr_error == total_nr_entries) {
        global_status = SCHEDULE_STATUS_ERROR;
    } else if(nr_disabled == total_nr_entries) {
        global_status = SCHEDULE_STATUS_DISABLED;
    } else if(nr_inactive == total_nr_entries) {
        global_status = SCHEDULE_STATUS_INACTIVE;
    } else if(nr_active > 0) {
        global_status = SCHEDULE_STATUS_ACTIVE;
    } else if(nr_inactive > 0) {
        global_status = SCHEDULE_STATUS_INACTIVE;
    }

skip_calculations:
    if(global_status != schedule->global_status) {
        schedule->global_status = global_status;
        schedule->cb(global_status, schedule->userdata);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}


static bool set_schedule_objects(schedule_t* schedule, const char* csv_path) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t paths;
    amxc_var_t lpaths;
    bool ret = false;

    amxc_var_init(&paths);
    amxc_var_init(&lpaths);

    when_str_empty(csv_path, exit);

    amxc_var_set(cstring_t, &paths, csv_path);
    amxc_var_convert(&lpaths, &paths, AMXC_VAR_ID_LIST);
    amxc_var_for_each(amx_path, &lpaths) {
        const char* path = GET_CHAR(amx_path, NULL);
        char* unterminated_path = get_terminated_path(path, false);
        schedule_object_t* obj = NULL;
        amxc_htable_it_t* it = NULL;

        obj = schedule_object_find(unterminated_path);
        if(obj == NULL) {
            // Create new schedule_object_t
            int res = schedule_object_new(&obj, unterminated_path);
            if(res != 0) {
                free(unterminated_path);
                SAH_TRACEZ_ERROR(ME, "Could not create internal schedule_object");
                goto exit;
            }
        }

        it = (amxc_htable_it_t*) calloc(1, sizeof(amxc_htable_it_t));
        if(it == NULL) {
            free(unterminated_path);
            SAH_TRACEZ_ERROR(ME, "Could not allocate htable iterator");
            goto exit;
        }
        amxc_htable_insert(&schedule->schedule_objects, unterminated_path, it);
        free(unterminated_path);
    }
    ret = true;

exit:
    amxc_var_clean(&lpaths);
    amxc_var_clean(&paths);
    SAH_TRACEZ_OUT(ME);
    return ret;
}

schedule_t* schedule_open(const char* csv_path, schedule_callback_t cb, void* userdata) {
    SAH_TRACEZ_IN(ME);
    schedule_t* ret = NULL;

    when_str_empty_trace(csv_path, exit, ERROR, "Empty path is given");
    when_null_trace(cb, exit, ERROR, "Callback cannot be NULL");

    ret = (schedule_t*) calloc(1, sizeof(schedule_t));
    when_null_trace(ret, exit, ERROR, "Failed to allocate schedule_t");

    amxc_htable_init(&ret->schedule_objects, 10);
    if(!set_schedule_objects(ret, csv_path)) {
        schedule_close(&ret);
        goto exit;
    }
    ret->global_status = SCHEDULE_STATUS_EMPTY;
    ret->cb = cb;
    ret->userdata = userdata;
    amxc_llist_it_init(&ret->it);
    add_schedule_to_schedule_list(ret);

    calculate_status(ret);

exit:
    SAH_TRACEZ_OUT(ME);
    return ret;
}

static void schedule_schedule_objects_it_delete(UNUSED const char* key, amxc_htable_it_t* it) {
    SAH_TRACEZ_IN(ME);
    free(it);
    SAH_TRACEZ_OUT(ME);
}

void schedule_close(schedule_t** schedule) {
    SAH_TRACEZ_IN(ME);
    when_null(*schedule, exit);
    when_null(schedule, exit);
    amxc_llist_it_take(&(*schedule)->it);

    amxc_htable_clean(&(*schedule)->schedule_objects, schedule_schedule_objects_it_delete);
    free(*schedule);
    *schedule = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
