# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.3 - 2024-05-09(07:47:18 +0000)

### Fixes

-  Error in setting ScheduleRef DM for port forwarding, pinhole and port trigger DM

## Release v0.1.2 - 2024-05-07(08:46:30 +0000)

### Fixes

- [libtr181-schedules] Better error handling when Schedules object is not found on the bus

## Release v0.1.1 - 2024-04-25(13:26:47 +0000)

### Other

- opensource component

## Release v0.1.0 - 2024-04-25(11:24:25 +0000)

### New

-  [tr181-schedules] Create library for managing subscriptions

